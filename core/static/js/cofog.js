/*
 * BubbleTree Style for COFOG taxonomy
 *
 */

if (!BubbleTree) window.alert('You must include the BubbleTree before including the styles');

BubbleTree.Styles = BubbleTree.Styles || {};

BubbleTree.Styles.Cofog = {
    '1': {
        icon: '/static/images/icons/dollar.svg',
        color: '#C75746'
    },
    '2': {
        icon: '/static/images/icons/transferencias.svg',
        color: '#0AB971'
    },
    '3': {
        icon: '/static/images/icons/gobierno-centralv2.svg',
        color: '#EC2406'
    },
    '4': {
        icon: '/static/images/icons/donaciones.svg',
        color: '#790586'
    },
    '5': {
        icon: '/static/images/icons/prestamosv4.svg',
        color: '#2A3A03'
    },
    '6': {
        icon: '/static/images/icons/otros.svg',
        color: '#D33673'
    }
};
